# README #
 tiamo 프로젝트함 :: 
 

### What is this repository for? ###
판매한 아이템을 입력하여 매출기록. 수금체크 기능, 매입기록, 영수증 출력기능 포함

### Quick summary ### 
	  개발환경 
	  CentOS Linux release 7.2
	  Python 2.7.5 
	  git 
	  mysql  5.6.34
	  django 1.11.29



## How do I get set up? ##

### Repo ###
	  git clone https://bitbucket.org/Syugagirl/tiamo_web.git


#### Install proper Django version ####

	  pip uninstall django # just for explicity
	  pip install django==1.11

#### Logs ####

* logfile path  /var/www/tiamo/log/log.log
* root path     /var/www/tiamo
* mysql log in live :/var/log/mariadb/mariadb.log

* location :: tiamo.settings.{env}

#### start django project ####     

    python manage.py runserver 0.0.0.0:8080  --settings=tiamo.settings.{env : dev | prod}



### requirements.txt guidelines
		
	  (essensial)
	  sudo pip install django-mysql
	  sudo pip install WhiteNoise==3.2
	  sudo pip install Unipath
	  sudo pip install django-model-utils==3.2.0
	  sudo pip install MySQL-python==1.2.5
	  
	  if using Anaconda 
			conda install -c conda-forge django-model-utils


### installed package (pip freeze)
    asgiref==3.2.3
    backports.functools-lru-cache==1.5
    certifi==2016.9.26
    Django==1.11.29
    django-model-utils==3.2.0
    MySQL-python==1.2.5
    pytz==2020.1
    sqlparse==0.3.1
    Unipath==1.1
    whitenoise==3.2
