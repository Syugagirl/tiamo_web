# -*- coding: utf8 -*-

from django.views.generic import  *
from django.views.generic.list import  MultipleObjectMixin 
 
from django.shortcuts import render
from mst.models import Client
from tiamo.models import Payment
from django.http import HttpResponse
import json

import logging
logger = logging.getLogger(__name__)

class Client_list(TemplateView):
	template_name  = 'client_list.html'
	context_object_name = 'recodes' 

	def get_context_data(self,**kwargs):
		context = super(Client_list, self).get_context_data(**kwargs)
		all_list = Client.objects.all().order_by('posnumber','segment')
		context['4flist'] = []
		context['3flist'] = []
		context['2flist'] = []
		context['1flist'] = []
		context['5flist'] = []
		context['6flist'] = []
		context['rlist'] = all_list
		# all_pData = Payment.objects.unpaidList_all()

		for client in all_list:
			obj = {}
			obj['unpaid_total'] = 0
			obj['applied_coupon'] = 0
			try:
				p_item = Payment.objects.get(client= client, paid_date__isnull = True)
				if p_item.unpaid_total :
					obj['unpaid_total'] += 	p_item.unpaid_total
				if p_item.remain > 0:
					obj['unpaid_total'] = p_item.remain + obj['unpaid_total']
				if p_item.applied_coupon :
					obj['applied_coupon'] += p_item.applied_coupon
			except Exception, e:
				pass
							
			obj['client'] = client


			posnumber= int(client.posnumber.split('-')[0])
			if posnumber >= 4000 and posnumber < 5000:
				context['4flist'].append(obj)
			elif posnumber >= 3000 and posnumber < 4000:
				context['3flist'].append(obj)
			elif posnumber >= 2000 and posnumber < 3000:
				context['2flist'].append(obj)
			elif posnumber >= 1000 and posnumber < 2000:
				context['1flist'].append(obj)
			elif posnumber >= 5000 and posnumber < 6000:
				context['5flist'].append(obj)
			elif posnumber >= 6000 and posnumber < 7000:
				context['6flist'].append(obj)

		context['title'] = "전체 거래처 목록"
		return context

	def post(self, request, **kwargs):
		all_list = Client.objects.all().order_by('posnumber','segment')
		dicdata = {}
		posnumList = []
		posData = []
		for item in all_list:
			posnum = item.segment + item.posnumber
			posnumList.append(posnum)
			client = {}
			client['name'] = item.name
			client['name_sub'] = item.name_sub
			client['posnumber'] = item.posnumber
			client['segment'] = item.segment
			client['contact01'] = item.contact01
			client['contact02'] = item.contact02
			client['payment_reserv_date'] = str(item.payment_reserv_date)
			client['receipt'] = item.receipt
			client['comment'] = item.comment
			posData.append(client)

		dicdata['posnumList'] = posnumList
		dicdata['posData'] = posData
		return HttpResponse(json.dumps(dicdata), content_type='application/json')
