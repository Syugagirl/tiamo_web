# -*- coding: utf-8 -*-
from django.contrib import admin
from mst.models import *
from django import forms


class Version(admin.ModelAdmin):
	list_display = ('ver','comment','modified','created')		
    
# Register your models here.
class Mst_sellerAdmin(admin.ModelAdmin):
	list_display = ('id', 'name', 'phone','comment','modified')

admin.site.register(Seller, Mst_sellerAdmin)

class Mst_materialAdmin(admin.ModelAdmin):
	list_display = ('id','name', 'count', 'unit_txt','price','seller', 'type','comment')
	search_fields = ['name','comment']
	# list_filter = ['seller','type']
admin.site.register(Material, Mst_materialAdmin)


class Mst_material_typeAdmin(admin.ModelAdmin):
	list_display = ('id', 'name', 'comment', 'modified')

admin.site.register(Material_type, Mst_material_typeAdmin)
  

class Mst_menu_typeAdmin(admin.ModelAdmin):
	list_display = ( 'id', 'name','name_sub', 'comment', 'modified')

admin.site.register(Menu_type, Mst_menu_typeAdmin)

 

class Mst_ProductAdmin(admin.ModelAdmin):
	list_display = ( 'id' , 'name','name_sub', 'menu_type','is_hot','size', 'selling_price' , 'cost_price') 
	list_filter = ['menu_type', 'is_hot', 'size']
	search_fields = ['name', 'name_sub']
	def get_ordering(self, request):
		return ['menu_type','name']
admin.site.register(Product, Mst_ProductAdmin)


class Mst_ClientAdmin(admin.ModelAdmin):
	# model = Client
	list_display = ( 'id', 'segment' ,'posnumber', 'name', 'name_sub', 'payment_type', 'payment_rules', 'receipt'
					, 'contact01', 'contact02' , 'comment' )
		# ) 
	list_filter = ('payment_type','segment')
	search_fields = ['posnumber','name','name_sub', 'id']

	def get_ordering(self, request):
		return ['segment' ,'-posnumber']	
admin.site.register(Client, Mst_ClientAdmin)


class Mst_Payment_rulesAdmin(admin.ModelAdmin):
	list_display = ('id', 'name','comment') 
admin.site.register (Payment_rules, Mst_Payment_rulesAdmin)

class Mst_Payment_typeAdmin(admin.ModelAdmin):
	list_display = ('id', 'name') 

admin.site.register (Payment_type, Mst_Payment_typeAdmin)







