# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from datetime import date
from model_utils.models import TimeStampedModel
import logging
logger = logging.getLogger(__name__)


        
class Seller(TimeStampedModel):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    phone = models.CharField(max_length=50)
    comment = models.TextField(max_length=200)

    class Meta(object):
        verbose_name = '매입처'
        verbose_name_plural = '매입처들'
    def publish(self):
        self.modified = timezone.now()
        #self.save()

    def __str__(self):
        return  str(self.id) + ". "+  self.name.encode('utf8')




class Material_type(TimeStampedModel):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    comment= models.CharField(max_length=200)
    created = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(default=timezone.now)

    class Meta(object):
        verbose_name_plural = '매입품목분류 항목'

    def publish(self):
        self.modified = timezone.now()

    def __str__(self):
        return str(self.id) + ". "+ self.name.encode('utf8')

class Material(TimeStampedModel):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200,blank=True)
    count = models.CharField(max_length=200, default = 1)
    price = models.CharField(max_length=200, default = 0)
    seller = models.ForeignKey('Seller', blank=True, default= 1)
    type = models.ForeignKey('Material_type', blank=True, default = 1)
    comment= models.CharField(max_length=200,blank=True)
    unit_txt = models.CharField(max_length=200, default = "ea") 

    class Meta:
        verbose_name_plural = '매입품목 관리' 
        ordering = ('id',)
        
    def publish(self):
        self.modified = timezone.now()
        #self.save()    
    
    def __str__(self):
	    return str(self.id) + ". "+  self.name.encode('utf8') + " " +str(self.price)

 
class Payment_type(TimeStampedModel):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100,verbose_name="타입명")

    class Meta:
        verbose_name ="보조 정책"
        verbose_name_plural = "수금보조 정책"

    def publish(self):
        self.modified = timezone.now()

    def __str__(self):
        return self.name.encode('utf8')

class Payment_rules(TimeStampedModel): 
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    comment = models.CharField(max_length=200,blank = True)

    class Meta:
        verbose_name ="정책 내용"
        verbose_name_plural = "정책 상세"

    def publish(self):
        self.modified = timezone.now()    
    def __str__(self):
        return  self.name.encode('utf8')


RECEIPT= (
    (0, '해당없음' ),
    (1, '무지영수증'),
    (2, '간이영수증'),
    )
PAYMENT_TYPE = (
    (20, '계좌이체'),
    (21, '지정요일'),
    (22, '즉시결제'),
    (23, '익일'),
    (24, '당일'),
    (25, '월말'),
    (26, '제한금액'),
    (30, '임의수금')
    )  

class ClientManager(models.Manager):
    def defer(self, client, defer_date):
        today = date.today()
        logger.info("%s defered payment %s"  % (client.name, defer_date))
        setattr(client, 'payment_reserv_date', defer_date)
        setattr(client, 'modified', today)
        client.save()

    def deferd_list(self, deferd_date):
        return Client.objects.filter(payment_reserv_date__isnull=False, modified = deferd_date)


class Client(TimeStampedModel):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length = 15, verbose_name="상호")
    name_sub = models.CharField(max_length = 10,blank = True, verbose_name="별칭")
    posnumber = models.CharField(max_length= 6, verbose_name = "호수")
    segment = models.CharField(max_length = 2 , blank = True, verbose_name = "동")
    contact01 = models.CharField(max_length = 45, blank = True, verbose_name= "연락처 1")
    contact02 = models.CharField(max_length = 45, blank = True, verbose_name= "연락처 2")
    payment_type = models.ForeignKey('Payment_type', blank = True, verbose_name="수금정책 분류")
    payment_rules = models.ForeignKey('Payment_rules', blank = True, default = 100,verbose_name="수금정책 내용")
    payment_reserv_date = models.DateField(null = True,blank=True, verbose_name="수금 보류(예약)")
    receipt = models.IntegerField(verbose_name="영수증 분류", blank = True, choices = RECEIPT, default = 0)
    comment = models.CharField(max_length=200,blank = True)

    class Meta:
        verbose_name = "거래처"
        verbose_name_plural = "거래처들"
        
    def __str__(self):
        return str(self.segment) +str(self.posnumber) 

    def as_dict(self):
        return {self.posnumber : {'id' : self.id, 'name':self.name, 'posnumber':self.posnumber}}
    
    objects = ClientManager()


class Menu_type(TimeStampedModel):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, verbose_name = "한글 분류명")
    name_sub = models.CharField(max_length=100, verbose_name = "영문 분류명")
    comment = models.CharField(max_length=200,blank = True, verbose_name = "설명")
    class Meta:
        verbose_name_plural = "판매상품분류 항목"

    def __str__(self):
        return self.name.encode('utf8')

IS_HOT=(
    ('1', 'hot'),
    ('2', 'ice'),
    )

SIZE= (
    ('1', 'small' ),
    ('2' , 'midi'),
    ('3', 'big'),
    )
 
class Product(TimeStampedModel): 
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=10, verbose_name="장부용")
    name_sub = models.CharField(max_length=10, verbose_name="정식명칭")
    menu_type = models.ForeignKey('Menu_type', verbose_name="분류항목")
    is_hot = models.CharField(max_length=1, choices=IS_HOT, blank= True)
    size = models.CharField(max_length=1, choices=SIZE, blank = True)
    selling_price = models.IntegerField(verbose_name="소비자가")
    cost_price = models.IntegerField(verbose_name="원가",blank = True)
    
    class Meta:
        verbose_name = "판매상품"
        verbose_name_plural = "판매상품들"
        
    def __str__(self):
        return self.name.encode('utf8')

class Version(TimeStampedModel):
    """docstring for Version"""
    id = models.AutoField(primary_key=True)
    ver = models.CharField(max_length=10, verbose_name="버전정보")
    comment = models.CharField(max_length=1000, verbose_name="업데이트 내용")

    class Meta:
        verbose_name="프로젝트 버전정보"
        db_table = 'version'