# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from mst.views import *

urlpatterns = [
 
    #Example: /mst/client/list/
    url(r'^client/list/$', Client_list.as_view(), name='client_list' ),

]
handler404 = 'mysite.views.my_custom_page_not_found_view'
handler500 = 'mysite.views.my_custom_error_view'
handler403 = 'mysite.views.my_custom_permission_denied_view'
handler400 = 'mysite.views.my_custom_bad_request_view'