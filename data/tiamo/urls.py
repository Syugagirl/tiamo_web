# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.contrib import admin

from tiamo.views import *
from django.conf import settings
admin.autodiscover()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),

    url(r'^tiamo/$', index_tiamo, name='home'),

    url(r'^mst/', include('mst.urls')),

    
###=============================================================================
###   통계 Stats
###=============================================================================
    # url: /tiamo/stats/
    # Example: tiamo/stats/
    url(r'^tiamo/stats/main/$', StatsMain.as_view(), name='stats_main'),
    


###=============================================================================
###   매입 / 구입 PURCHASE 
###=============================================================================
  
    # Example: /
    # url(r'^$', WelcomeView()),
    # Example: tiamo/purchase/
    url(r'^tiamo/purchase/main/$', PurchaseMainView.as_view(), name='purchase_main'),

    #Example: tiamo/purchase/2017/02/16/
    url(r'^tiamo/purchase/list/(?P<date>[0-9]{8})/$', PurchaseDayListView.as_view(), name='purchase_list'),

    # Example: tiamo/purchase/create/
    url(r'^tiamo/purchase/create/$', PurchaseCreateView.as_view(), name='purchase_create'),

    # Example: tiamo/purchase/10/update/
    url(r'^tiamo/purchase/(?P<pk>\d+)/update/$', PurchaseUpdateView.as_view(), name='purchase_update'),

    # Example: tiamo/purchase/10/delete/
    #url(r'^tiamo/purchase/(?P<pk>\d+)/delete/$', PurchaseDeleteView.as_view(), name='purchase_delete'),
    
    # Example: tiamo/purchase/10/
    url(r'^tiamo/purchase/(?P<pk>\d+)/$', PurchaseDetailView.as_view(), name='purchase_detail'),
    

###=============================================================================
###   판매, 막장부 , 수금장부 ORDERS 
###=============================================================================

    ##판매현황(조회일)
    #Example: tiamo/order/status/
    url(r'^tiamo/order/status/$', OrderStatus.as_view() , name='order_main'),

    ##판매현황(지정일자)
    #Example: tiamo/order/status/20170503/
    url(r'^tiamo/order/status/(?P<date>[0-9]{8})/$', OrderStatus.as_view() , name='order_main'),

    # ##판매현황(지정일자)
    # #Example: tiamo/order/status/20170503/
    # url(r'^tiamo/order/status/(?P<date>[0-9]{8})/$', OrderStatus.as_view() , name='order_main'),

    ##막장부  판매기록 신규입력
    #Example: tiamo/order/
    url(r'^tiamo/order/$', CreateOrder.as_view() , name='create_order'),
 
    ##막장부  판매기록 업데이트
    #Example: tiamo/order/update
    url(r'^tiamo/order/update/$', UpdateOrder.as_view() , name='update_order'),


    # ##막장부
    # #Example: tiamo/order/list/
    url(r'^tiamo/order/list/log/(?P<date>[0-9]{8})/$', OrderLogByDate.as_view(), name='order_log_daily') ,

    ##막장부(지정일자)
    #Example: tiamo/order/list/20170503/
    url(r'^tiamo/order/list/(?P<date>[0-9]{8})/$', OrderListByDate.as_view(), name='order_daily_list') ,
   
    # ##메뉴명 유효성 검사
    # #Example: validation/menuname/
    url(r'^validation/menuname/$', ValidateOrderMenuname.as_view() , name='menuname_check'),


    ##(판매)거래내역
    #Example: tiamo/order/history/1/
    url(r'^tiamo/order/history/(?P<client_id>[0-9])/$', OrderHistoryByClient.as_view(), name='order_history') ,
    
    url(r'^tiamo/order/history/$', OrderHistoryByClient.as_view(), name='order_history') ,

    #일일 거래내역 가져오기 
    #POST: client_id, date
    url(r'^tiamo/order/product_txt/$', TodaysOrderText.as_view(), name='todays_orders') ,
    
    
###=============================================================================
###   외상 / 수금 Payment 
###=============================================================================

    ## 누적미수금액 전체 재계산   
    url(r'^tiamo/payment/recalc/all/(?P<date>[0-9]{8})/$', PaymentRecalc.as_view(), name='payment_recalc' ),
 
    ##외상현황
    ##Example: /tiamo/payment/
    url(r'^tiamo/payment/$', PaymentState.as_view(), name='payment_state' ),
 
    ##일일막장부리스트
    #Example: tiamo/order/list/20170503/
    url(r'^tiamo/payment/(?P<date>[0-9]{8})/$', PaymentState.as_view(), name='payment_state') ,

    ##수금대상
    ##Example: /tiamo/payment/plan/
    url(r'^tiamo/payment/plan/$', PaymentPlan.as_view(), name='payment_list') ,
    
    ##일일수금대상
    ##Example: /tiamo/payment/plan/20170903/ 
    url(r'^tiamo/payment/plan/(?P<date>[0-9]{8})/$', PaymentPlan.as_view(), name='payment_list'),


    ##정산입력
    ##Example: /tiamo/payment/time/
    url(r'^tiamo/payment/time/$', Floor_list.as_view(), name='payment_time'),

    ##정산입력
    ##Example: /tiamo/payment/time/
    url(r'^tiamo/payment/time/(?P<date>[0-9]{8})/$', Floor_list.as_view(), name='payment_time'),

    ##수금처리
    ## POST
    ##Example: /tiamo/payment/payment/received/
    url(r'^tiamo/payment/received/$', Payment_received.as_view(), name="payment_received"),

 
    ##수금 연기 처리
    ## POST
    ##Example: /tiamo/payment/payment/received/
    url(r'^tiamo/payment/defer/$', Payment_defered.as_view(), name="payment_defered"),


    # ##기간 외 수금처리 대상 선택 
    # url(r'^tiamo/payment/outdate/(?P<posnum>[A-Z]{1}[0-9]{4})/$', Outdate_payment_select.as_view(), name="outdate_selected"),


    


]

# if settings.DEBUG:
#     import debug_toolbar
#     urlpatterns = [
#         url(r'^__debug__/', include(debug_toolbar.urls)),
#     ] + urlpatterns

from django.conf.urls import (
    handler400, handler404, handler500
)
handler400 = 'django.views.defaults.bad_request'
handler404 = 'tiamo.views.page_not_found_page'
handler500 = 'tiamo.views.server_error_page'
