# -*- coding: utf8 -*-
from django.views.generic import ListView, DetailView, CreateView , UpdateView, FormView, TemplateView
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse_lazy ,reverse
from django.conf import settings
from django.http import HttpResponse ,HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from datetime import date ,datetime
from tiamo.models import PurchaseDetail, Sales, Payment, Orders
from mst.models import Version, Client
from tiamo.forms import *
from django.db.models import Max
import json
import logging
import re
logger = logging.getLogger(__name__)
def index_tiamo(request):
	info = Version.objects.get(id = 1)
	
	context={'lastest':info.modified, 'comment': info.comment, 'versions': info.ver, 'created': info.created}
	return render(request,'index.html', context)

# 400 에러(Error)
def page_not_found_page (request):
	context = {}
	response = render(request, '404.html', context)
	response.status_code = 404
	return response

# 500 에러(Error)
def page_not_found_page (request):
	context = {}
	response = render(request, '500.html', context)
	response.status_code = 500
	return response


###=============================================================================
###   STATS 
###=============================================================================
class StatsMain(TemplateView):
	model = PurchaseDetail
	template_name  = 'stats/main.html'
	# context_object_name = 'recodes' 

	def get_context_data(self,**kwargs):
		context = super(StatsMain, self).get_context_data(**kwargs)
		

###=============================================================================
###   PURCHASE 
###=============================================================================
class PurchaseMainView(TemplateView):
	model = PurchaseDetail
	template_name  = 'purchase/purchase_main.html'
	# context_object_name = 'recodes' 

	def get_context_data(self,**kwargs):
		context = super(PurchaseMainView, self).get_context_data(**kwargs)
		
		if self.kwargs.has_key('month'):
			context['month'] = self.kwargs['month']
			# context['recodes'] = PurchaseDetail.objects.filter(orderdate__gte=date)
		else:
			context['month'] = date.today().month
			# context['recodes'] = PurchaseDetail.objects.all().order_by('-orderdate')[:100]

		#매입처
		sellers = Seller.objects.all().order_by('id')
		#매입처별 누적 구매금액 
		total_prices = PurchaseDetail.objects.accrued_total(sellers, context['month'] )
		#상품별 누적 구매금액 
		materials = ''
		
		# context['sellers'] = sellers
		context['total_prices'] = total_prices	
		context['materials'] = materials
		context['title'] = str(context['month']) +"월 매입현황"
		 
 		return context



class PurchaseListView(ListView):
	model = PurchaseDetail
	template_name  = 'purchase/purchase_list.html'
	context_object_name = 'recodes' 

class PurchaseCreateView(FormView):
 	model = PurchaseDetail
 	template_name = 'purchase/purchase_create_form.html'

	def get(self, request, *args, **kwargs): 
		# form = self.form_class(initial=self.initial)
		sellers = Seller.objects.all()
		materials = Material.objects.all()
		js_materials = []

		for item in materials:
			mat_obj = {}
			sel_obj = {}
			sel_obj['id'] = item.seller.id
			sel_obj['name'] = item.seller.name

			mat_obj['seller'] = sel_obj
			mat_obj['id'] = item.id
			mat_obj['name'] = item.name
			mat_obj['price'] = item.price
			js_materials.append(mat_obj)
		context={'sellers':sellers, 'materials': materials, 'today': date.today().strftime('%Y-%m-%d'), 'js_materials':json.dumps(js_materials)}

		return render(request, self.template_name, context)

	def post(self, request, *args, **kwargs):
		error = False
		msg = ''
		############ param validate ############
		if not request.POST['date']:
			p_date = date.today().strftime('%Y%m%d')

		p_date_str  =re.sub(r'[^0-9]', '',request.POST['date'] )
		p_date = filter(str.isdigit, str(p_date_str))

		if not request.POST['material']:
			msg = "error ! There was no material param."
			error = True
		
		p_material = Material.objects.get(id= request.POST['material'])

		if not request.POST['seller']:
			msg = "error ! There was no seller param."
			error = True
		p_seller = Seller.objects.get(id= request.POST['seller'])

		if not request.POST['count']:
			msg = "error ! There was no count param."
			error = True
		p_count = int(request.POST['count'])
		if p_count <= 0 :
			msg = "Opps! count can not be smaller than 1."
			error = True

		if not request.POST['unit_price']:
			msg = "error ! There was no Unit price param."
			error = True

		p_unit_price = int(request.POST['unit_price'])
		if p_unit_price <= 0 :
			error = True
			msg = "Unit price can not be smaller than 1. Then, change total_price."

		if not request.POST['total_price']:
			msg = "error ! There was no total_price param."
			error = True
		p_total_price = request.POST['total_price']

		if not request.POST.has_key('comment'):
			msg = "error ! There was no comment param."
			error = True
		p_comment = request.POST['comment']

		############ save PurchaseDetail ############
		if not error :
			try:
				PurchaseDetail.objects.create(material= p_material, seller= p_seller , count= p_count, now_unit_price= p_unit_price
					, total_price= p_total_price, comment= p_comment, orderdate= str_to_date(p_date))
			except Exception, e:
				raise e

		############ make context data ############
		sellers = Seller.objects.all()
		materials = Material.objects.all()
		js_materials = []

		for item in materials:
			mat_obj = {}
			sel_obj = {}
			sel_obj['id'] = item.seller.id
			sel_obj['name'] = item.seller.name
			mat_obj['seller'] = sel_obj
			mat_obj['id'] = item.id
			mat_obj['price'] = item.price
			js_materials.append(mat_obj)

		context = {}	
		context['error'] = error
		context['msg'] = msg
		context['today'] = p_date
		try:
			result_data = json.dumps(context)
		except Exception, e:
			raise e
		return HttpResponse(result_data, content_type='application/json')

		


class PurchaseUpdateView(UpdateView):
	model = PurchaseDetail
	fields = ['material','seller', 'count','now_unit_price','total_price','comment']


class PurchaseDetailView(DetailView): 
	# queryset = PurchaseDetail.objects.all()
 	template_name = 'purchase/purchase_detail.html'
 	context_object_name = 'recode'
 	def get_queryset(self):
		return get_object_or_404(PurchaseDetail, id = self.kwargs['pk'])


class PurchaseDayListView(ListView):
	template_name  = 'purchase/purchase_list.html'
	model = PurchaseDetail

	def get_context_data(self,**kwargs):
		context = super(PurchaseDayListView, self).get_context_data(**kwargs)
		
		if self.kwargs.has_key('date'):
			date = str_to_date(self.kwargs['date'])
			context['date'] = date
			context['title'] =date.strftime('%Y.%m.%d') +" 매입현황"
			context['recodes'] = PurchaseDetail.objects.filter(orderdate__gte=date)
		else:
			# context['date'] = date
			context['title'] ="최근 100건 "
			context['recodes'] = PurchaseDetail.objects.all().order_by('-orderdate')[:100]	
 		return context

###=============================================================================
###   ORDERS 
###=============================================================================

#Example: tiamo/order/status/
class OrderStatus(ListView):
	template_name= 'order/order_main.html'
	model = Sales

	def get_context_data(self,**kwargs):
		re_calc_data = True
		context = super(OrderStatus, self).get_context_data(**kwargs)

		if self.kwargs.has_key('date'):
			_date = str_to_date(self.kwargs['date'])
		else:
			_date = date.today()
		
		if self.kwargs.has_key('is_reCalc'):
			re_calc_data = True

		context['title'] = "일일 판매현황"
		context['date']  = _date.strftime('%Y%m%d')
		context['date_name']  = _date.strftime('%Y.%m.%d') + "("+ get_date_name(_date.isoweekday()) +")"
		context['date_params_before'] = []
		context['date_params_after'] = []
		context['today_total_sales'] = 0
		context['cash_total'] = 0

		for i in range(1,6):
			obj = {}
			target_date = _date - datetime.timedelta(i)
			day_text = "("+ get_date_name(target_date.isoweekday()) +")"
			if is_business_day(target_date) :
				obj['date'] = target_date.strftime('%Y%m%d')
				obj['name'] = target_date.strftime('%Y.%m.%d') + day_text
				context['date_params_before'].append(obj)

		for i in reversed(range(1,4)):
			obj = {}
			target_date = _date + datetime.timedelta(i)
			day_text = "("+ get_date_name(target_date.isoweekday()) +")"
			if is_business_day(target_date) :
				obj['date'] = target_date.strftime('%Y%m%d')
				obj['name'] = target_date.strftime('%Y.%m.%d') + day_text
				context['date_params_after'].append(obj)

		context['rlist'] = []
		# year =  int(today[0:4] )
		# month=  int(today[4:6] )
		# day  =  int(today[6:] )

		# orderDatas = Orders.objects.get_OrderData_all(_date, None)
		orderDatas = Sales.objects.get_OrderData_all_clients(_date)
		applied_coupon = 0
		result_data = []
		for o_item in orderDatas:
			context['today_total_sales'] += o_item.selling_price
			if o_item.client.id == 212:
				context['cash_total'] += o_item.selling_price
			obj = {}
			obj['unpaid_total']  = 0
			obj['applied_coupon'] = 0
			obj['client'] = o_item.client
			obj['products'] = o_item.menuname
			obj['total_sales'] = o_item.selling_price
			obj['received'] = o_item.received

			p_data = Payment.objects.get_item(o_item.client)
			if p_data :

				if p_data.applied_coupon :	
					obj['applied_coupon'] = p_data.applied_coupon
				if p_data.remain : 
					obj['unpaid_total'] += p_data.remain

				if p_data.unpaid_total :
					if o_item.received == False :
						obj['unpaid_total'] += p_data.unpaid_total
			result_data.append(obj)

		context['rlist'] = result_data
		return context


## TODO. 사용안함
#Example: tiamo/order/list/
class OrderAllListView(ListView):
	template_name = 'order/orders_list.html'
	model = Orders

	def get_context_data(self,**kwargs):
		context = super(OrderAllListView, self).get_context_data(**kwargs)
		context['title'] = "최근 거래상세 (최종 10개 임시표시)"
		context['rlist'] = self.model.objects.all().order_by('-orderdate')[:10]		
		return context


# 일일 판매기록(막장부) 
#Example: /tiamo/order/list/log/20170503
class OrderLogByDate(ListView):
	template_name = 'order/orders_list_log.html'
	model = OrdersLog

	def get_context_data(self, **kwargs):
		context = super(OrderLogByDate, self).get_context_data(**kwargs)

		## side nav list
		date_str = self.kwargs['date']
		_date = str_to_date(date_str)
		date_after = _date + datetime.timedelta(1)
		date_before= _date - datetime.timedelta(1)
		context['title'] = _date.strftime('%Y.%m.%d') + "일자 막장부"
		context['date']  = _date.strftime('%Y%m%d')
		context['date_name']  = _date.strftime('%Y.%m.%d') + "("+ get_date_name(_date.isoweekday()) +")"
		context['date_params_before'] = []
		context['date_params_after'] = []

		for i in range(1,6):
			obj = {}
			target_date = _date - datetime.timedelta(i)
			day_text = "("+ get_date_name(target_date.isoweekday()) +")"
			if is_business_day(target_date) :
				obj['date'] = target_date.strftime('%Y%m%d')
				obj['name'] = target_date.strftime('%Y.%m.%d') + day_text
				context['date_params_before'].append(obj)

		for i in reversed(range(1,4)):
			obj = {}
			target_date = _date + datetime.timedelta(i)
			day_text = "("+ get_date_name(target_date.isoweekday()) +")"
			if is_business_day(target_date) :
				obj['date'] = target_date.strftime('%Y%m%d')
				obj['name'] = target_date.strftime('%Y.%m.%d') + day_text
				context['date_params_after'].append(obj)
		
		## get data
		context['rlist'] = OrdersLog.objects.filter(orderdate = _date)
		
		context['total_raw_count'] = len(context['rlist'])
		return context

# 일일 판매이력 
#Example: /tiamo/order/list/20170503
class OrderListByDate(ListView):
	template_name = 'order/orders_list.html'
	model = Orders

	def get_context_data(self, **kwargs):
		context = super(OrderListByDate, self).get_context_data(**kwargs)
		
		## side nav list
		date_str = self.kwargs['date']
		_date = str_to_date(date_str)
		date_after = _date + datetime.timedelta(1)
		date_before= _date - datetime.timedelta(1)
		context['title'] = _date.strftime('%Y.%m.%d') + "일자 판매이력"
		context['date']  = _date.strftime('%Y%m%d')
		context['date_name']  = _date.strftime('%Y.%m.%d') + "("+ get_date_name(_date.isoweekday()) +")"
		context['date_params_before'] = []
		context['date_params_after'] = []

		for i in range(1,6):
			obj = {}
			target_date = _date - datetime.timedelta(i)
			day_text = "("+ get_date_name(target_date.isoweekday()) +")"
			if is_business_day(target_date) :
				obj['date'] = target_date.strftime('%Y%m%d')
				obj['name'] = target_date.strftime('%Y.%m.%d') + day_text
				context['date_params_before'].append(obj)

		for i in reversed(range(1,4)):
			obj = {}
			target_date = _date + datetime.timedelta(i)
			day_text = "("+ get_date_name(target_date.isoweekday()) +")"
			if is_business_day(target_date) :
				obj['date'] = target_date.strftime('%Y%m%d')
				obj['name'] = target_date.strftime('%Y.%m.%d') + day_text
				context['date_params_after'].append(obj)

		# get data
		context['rlist'] = Orders.objects.filter(orderdate = _date)
		#화면에 표시 될 내용은 상호 , 주문내용 (menuname, 그룹핑된것 ), 비고 (comment) 이면 된다 
		context['total_raw_count'] = len(context['rlist'])
		return context


## 거래내역 
#Example: tiamo/order/history/(client_id)/
class OrderHistoryByClient(ListView):
	template_name = 'order/order_main.html'
	model = Orders

	def get_context_data(self, **kwargs):
		## ajax 요청일 경우 구현해야함 
		############
		context = super(OrderHistoryByClient, self).get_context_data(**kwargs)
		 
		if self.kwargs.has_key('client_id'):
			client_id = self.kwargs['client_id']
			client = Client.objects.get(id = client_id)
		else:
			logger.error("client_id가 있어야 조회가능 ")
			return context

		if self.kwargs.has_key('until'):
			date_until = str_to_date(self.kwargs['until'])
		else:
			date_until = date.today()

		result_data = Orders.objects.get_order_history_by_client_id(client, date_until)
		 
		 
		context['title'] = "외상 거래내역"
		context['date'] = start_date.strftime('%Y%m%d')
		context['rlist'] = result_data
		
		return context

	def post(self, request, *args, **kwargs):
		result_data = {}
		result_data['msg'] = 'success'
		result_data['result'] = True
		end_date = None
		start_date = None
		last_payday = ''


		if request.POST.has_key('client_id'):
			client_id = request.POST['client_id']
		else:
			result_data['msg'] =  "client_id가 있어야 조회가능 "
			result_data['result'] = False
			# logger.error(result_data['msg'])
			return HttpResponse(json.dumps(result_data), content_type='application/json')

		if request.POST.has_key('until'):
			date_until = str_to_date(request.POST['until'])
			# date_until =  date_until- datetime.timedelta(days=1)
		else:
			date_until = date.today()

		# get client_id
		client = Client.objects.get(id = client_id)

		# get order datas
		try:
			# 판매내역 조회
			history_data = Orders.objects.get_order_history_by_client_id(client, date_until)
		except Exception, e:
			result_data['result'] = False
			result_data['msg'] = "Orders tables have no data : c_id ("+str(client_id)+") "
			
			logger.error( '%s (%s)' % (e.message, type(e)) )
			return HttpResponse(json.dumps(result_data), content_type='application/json')

		# 이전 수금일 조회
		last_payday = Payment.objects.last_payday(client)

		if last_payday is not None : 
			last_payday = last_payday.strftime('%Y%m%d')
		result_data['lastest_paiddate'] = last_payday
		# 수금 후 잔액 
		result_data['remain'] = Payment.objects.remain(client)
		result_data['title'] = "외상 거래내역"
		result_data['history'] = history_data
		return HttpResponse(json.dumps(result_data), content_type='application/json')


class TodaysOrderText(TemplateView): 
	template_name = 'order/order_addData.html'
	model = Orders
	def post(self, request, *args, **kwargs):
		result_data = {}
		result_data['msg'] = 'success'
		result_data['result'] = True	
		if request.POST.has_key('client_id'):
			client_id = request.POST['client_id']
		try:
			# get client_id
			client = Client.objects.get(id = client_id)
		except Exception, e:
			result_data['msg'] =  "client_id가 있어야 조회가능 "
			result_data['result'] = False
			# logger.error(result_data['msg'])
			return HttpResponse(json.dumps(result_data), content_type='application/json')

		if request.POST.has_key('date'):
			today = str_to_date(request.POST['date'])
		else:
			today = date.today() 

		# get order datas
		try:
			# 판매내역 조회
			history_data = Orders.objects.get_product_text_today(client, today)

		except Exception, e:
			result_data['result'] = False
			result_data['msg'] = "Orders tables have no data : c_id ("+str(client_id)+") "
			logger.error( '%s (%s)' % (e.message, type(e)) )
			return HttpResponse(json.dumps(result_data), content_type='application/json')

		result_data['history'] = history_data
		return HttpResponse(json.dumps(result_data), content_type='application/json')


class CreateOrder(TemplateView):
	template_name = 'order/order_addData.html'
	model = Orders
	def get(self, request, *args, **kwargs): 
		# form = self.form_class(initial=self.initial)
		return render(request, self.template_name)

	def post(self, request, *args, **kwargs):

		############ param validate #########
		if not request.POST['date']:
			p_date = date.today().strftime('%Y%m%d')
		else:
			# p_date = (request.POST['date']).replace('-','').encode('utf-8')
	 		p_date = (request.POST['date']).replace('-','')

	 		
		if request.POST.has_key('menuname[]') == False:
			return HttpResponse('menuname[] 키가 존재하지 않음')

		###############
 		p_data = dict(request.POST.iterlists())

		data_cnt = 	len(p_data['menuname[]'])

		for i in range( 0 , data_cnt):
			if p_data['client[]'][i] != '' :
				t_client = p_data['client[]'][i]
			else:
				break;

 			if p_data['menuname[]'][i] != '':
				t_menuname = p_data['menuname[]'][i] 

			t_comment = ''			
 			try:
				order_dataset = Orders.objects.makeOrderDataSet(t_client, t_menuname , t_comment, str_to_date(p_date)) 
			except Exception, e:
 				print "validation error -"
 				print str(t_client)
				print (t_menuname).encode('utf8')
				print str(t_comment)
				print str(p_date)
				raise e
 				# logger.warn("validation error -  %s, %s, %s" % (t_client, t_menuname , t_comment, p_date))
			
			OrdersLog.objects.insert_orders_log(t_client, t_menuname , t_comment, str_to_date(p_date))

 			logger.info("New Order data requested  - %s  %s" % (t_client , t_menuname))
 			Orders.objects.upsertOrderData(order_dataset)

		url = reverse('create_order')
		return HttpResponseRedirect(url)

class UpdateOrder(TemplateView):
	template_name = 'order/order_main.html'
	model = Orders

	def post(self, request, *args, **kwargs):
		result_data = {}
		result_data['msg'] =''
		result_data['result'] = True
		if request.POST.has_key('client_id'):
			client_id = request.POST['client_id']

		if request.POST.has_key('target_date'):
			target_date = str_to_date(request.POST['target_date'])

		if request.POST.has_key('products'):
			products_txt = request.POST['products']

			
		try:
			result_data['today_total'] =  Orders.objects.update_datas(client_id , target_date, products_txt)
			result_data['unpaid_total'] = 0
			result_data['coupon_total'] = 0
		except Exception, e:
			result_data['msg'] = "client.id : %d update error" % (int(client_id))
 			result_data['result'] = False	
 			logger.error( "error %d , %s  " % (int(client_id), products_txt) )
			logger.error('%s (%s)' %( e.message, type(e)) )

		# 막장부 입력기록 삭제 후 업데이트
		OrdersLog.objects.delete_orders_log(client_id, target_date)
		OrdersLog.objects.insert_orders_log(client_id, products_txt,'수정됨', target_date)


		p_data = Payment.objects.get(client_id = client_id, paid_date__isnull = True)
		if p_data.unpaid_total:
			result_data['unpaid_total'] += p_data.unpaid_total
			result_data['unpaid_total'] += p_data.remain
		if p_data.applied_coupon :
			result_data['coupon_total'] = p_data.applied_coupon 

 		return HttpResponse(json.dumps(result_data), content_type='application/json')

  

class ValidateOrderMenuname(TemplateView):
 	template_name = 'index.html'
 	msg = 'validation check result message.'


	def post(self, request, *args, **kwargs): 
 		result_data = {}
 		result_data['data'] = 'false'
 		result_data['id'] = ''
 		result_data['msg'] = 'success'
		
		if not request.POST.has_key('data'): 
			result_data['msg'] = '입력한 데이터가 없습니다.'
			return HttpResponse(json.dumps(result_data), content_type='application/json')
		if not request.POST.has_key('id'): 
			msg = '검증대상 id가 없습니다.'
			return HttpResponse(json.dumps(result_data), content_type='application/json')
		
		_menunames = request.POST['data']
		result_data['id'] =  request.POST['id']

		# 총 판매금액 계산
		total = 0
		try: 
			product_list = Orders.objects.validateMenuNames(_menunames)
			for item in product_list:
			
				if item[2] == '':
					total += item[0].selling_price * int(item[1])

		except SyntaxError, e:
			# result_data['msg'] = "메뉴명은 숫자로 끝날 수 없습니다. 수량은 공백으로 구분되어야 합니다" = e
			result_data['msg'] = str(e)
			return HttpResponse(json.dumps(result_data), content_type='application/json')
			
		except Exception, e: 
			result_data['msg'] = str(e)
			# result_data['msg'] = '등록되지 않은 상품명이 있습니다. 혹은 규칙에 위배되는지 확인하세요. 설반2 (X)  설반 2 (0)'
			return HttpResponse(json.dumps(result_data), content_type='application/json')


		result_data['total'] = total
		result_data['data'] = 'true'
		return HttpResponse(json.dumps(result_data), content_type='application/json')

# 외상현황
class PaymentState(ListView): 

	template_name = 'payment/payment_state.html'
	model = Payment	

	def get_context_data(self, **kwargs):
		context = super(PaymentState, self).get_context_data(**kwargs)

		if self.kwargs.has_key('date'):
			# 요청한 날짜보다 하루 전까지의 총 판매액을 미수금이라고 정의한다.
			param_date = str_to_date(self.kwargs['date'])
			_endDay = str_to_date (param_date) - datetime.timedelta(days=1)
 		else:
			param_date = date.today()
			_endDay = param_date - datetime.timedelta(days=1)

		context['title'] = param_date.strftime('%Y.%m.%d') +"일자 외상장부"
		context['title_help']= "미수금액이 있는 모든 거래처를 표현합니다.      미수금 : 마지막 수금일 (혹은 처음) ~ " + str(param_date) + " (- 1)  일자까지의 판매액."
		context['day'] = str(param_date.strftime('%d'))
		context['date'] = param_date.strftime('%Y%m%d')
		unpaidList = Payment.objects.unpaidList_all( target_date = _endDay , _client = None)  

		context['plist'] = unpaidList
		return context


# 수금계획
## 관리자 화면용 수금대상 리스트 
class PaymentPlan(ListView):
	template_name='payment/payment_state.html'
	model = Payment

	def get_context_data(self, **kwargs):
		context = super(PaymentPlan, self).get_context_data(**kwargs)
		if self.kwargs.has_key('date'):
			# 요청한 날짜까지의 총 판매액을 미수금이라고 정의한다.
			# 요청한 날짜짜보다 다음날 의 수금 기준에 해당되는 애들을 필터링 .
			param_date = str_to_date (self.kwargs['date'])
			logger.info("날짜 지정됨 - %s" % param_date)
 		else:
			param_date = date.today()

		context['title'] =  param_date.strftime('%Y.%m.%d') +"일자  수금대상"

		context['title_help']= "수금 대상이 되는 거래처를 표현합니다. ~"+param_date.strftime('%d')+" 까지의 데이터 로 내일 혹은 다음주 월에 해당하는 거래처 대상 ."
		context['day'] = str(param_date.strftime('%d'))
		context['edit_data_flag'] = True # 리스트 편집버튼 플래그
		context['date'] = param_date.strftime('%Y%m%d')
		unpaidList = Payment.objects.unpaidList_filterd( param_date ) 

		context['plist'] = unpaidList
		return context

# 층별(수금계획)
## 모바일화면용 수금대상 리스트
## 수금이 완료된 항목은 '수금완료 일자 기준으로 오늘 인 애들을 추가하여 출력하자'
class Floor_list(ListView):
	template_name='payment/payment_time_list.html'
	model = Payment

	def get_context_data(self, **kwargs):
		context = super(Floor_list, self).get_context_data(**kwargs)
		today = date.today()

		if self.kwargs.has_key('date'):
			# 수금하는 날자를 지정하면 그 전날 까지의 거래금액까지만 조회한다.
			today = str_to_date (self.kwargs['date'])

		# _endDay = today - datetime.timedelta(days=1)

		unpaidList = Payment.objects.unpaidList_filterd( today )

		_flist = [] 
		_posnumList = []
		_deferdList = Client.objects.deferd_list(today)

		context['4flist'] = []
		context['3flist'] = []
		context['2flist'] = []
		context['1flist'] = []
		context['5flist'] = []
		context['6flist'] = []

		for data in unpaidList:
			posnumber= int(data['client'].posnumber.split('-')[0])
			if data['client'] in _deferdList:
				unpaidList.remove(data) 
			if posnumber >= 4000 and posnumber < 5000:
				context['4flist'].append(data)
			elif posnumber >= 3000 and posnumber < 4000:
				context['3flist'].append(data)
			elif posnumber >= 2000 and posnumber < 3000:
				context['2flist'].append(data)
			elif posnumber >= 1000 and posnumber < 2000:
				context['1flist'].append(data)
			elif posnumber >= 5000 and posnumber < 6000:
				context['5flist'].append(data)
			elif posnumber >= 6000 and posnumber < 7000:
				context['6flist'].append(data)


		context['date'] = today
		context['paidlist'] = Payment.objects.paidList(today)
		context['deferd_list'] = _deferdList
		return context
 
# 수금처리 
## 모바일 화면용 
class Payment_received(TemplateView):
	template_name="index.html"
	def post(self, request, **kwargs):
		result_data = {}
		result_data['msg'] = 'success'
		result_data['result'] = False

		if not request.POST.has_key('client_id'):
			result_data['msg'] =  "client id 가 없음.."
			logger.error(result_data['msg'])

		# client_id + 요청을 받은 날짜 기준으로 수금처리를 진행한다.
		# 수금처리 후 결과를 반환한다. 
		client_id = request.POST['client_id']
		client = Client.objects.get( id= client_id)


 		if request.POST.has_key('payment_amount') :
	 		#수금일 
	 		payday = date.today()
			# 받은 금액만큼만 수금처리
			paid_amount = request.POST['payment_amount']
			
			result_data['remain'] = Payment.objects.received_by_paid_amount(client, int(paid_amount), payday)
		elif request.POST.has_key('until_date') :
			#지정한 날짜까지 수금처리 
 			_date =  (request.POST['until_date']).replace('-','').encode('utf-8')
	 		
	 		try:
				year  = int(_date[0:4])
				month = int(_date[4:6])
				day   = int(_date[6:])
	 			
	 		except Exception, e:
	 			print e
	 			result_data['msg'] = "날짜 형식이 잘 못 되었습니다 ex)yyyy-mm-dd"
				return HttpResponse(json.dumps(result_data), content_type='application/json')

			try:
				payday = date(year,month,day)
			except Exception, e:
				result_data['msg'] = "date() type 변환 불가"
				raise e
 			# 어제까지 판매한것을 모두 수금처리 함. 
			result_data['remain'] = Payment.objects.received_untill(client, payday)

		else:
	 		#수금일 
	 		payday = date.today()			
 			# 어제까지 판매한것을 모두 수금처리 함. 
			result_data['remain'] = Payment.objects.received_untill(client, get_yesterday(payday))

		return HttpResponse(json.dumps(result_data), content_type='application/json')


## 지불 연기 처리
class Payment_defered (TemplateView):
	template_name="index.html"
	
	def post(self, request, **kwargs):
		result_data = {}
		result_data['msg'] = 'success'
		result_data['result'] = True
		if not request.POST.has_key('defer_date'):
			result_data['msg'] =  "DEFER_DATE key is missing."
			result_data['result'] = False
			logger.error(result_data['msg'])
			return HttpResponse(json.dumps(result_data), content_type='application/json')
		if not request.POST.has_key('client_id'):
			result_data['msg'] =  "CLIENT_ID key is missing."
			result_data['result'] = False
			logger.error(result_data['msg'])
			return HttpResponse(json.dumps(result_data), content_type='application/json')
		try:
			client_id = request.POST['client_id']
			client = Client.objects.get( id= client_id)

			defer_date = str_to_date(request.POST['defer_date'].replace('-',''))
			Client.objects.defer(client, defer_date)
			
		except Exception, e:
			result_data['msg'] =  "client_id, defer_date isn't available."
			result_data['result'] = False
			return HttpResponse(json.dumps(result_data), content_type='application/json')			

		return HttpResponse(json.dumps(result_data), content_type='application/json')

 
## 누적 미수금 계산
## 날짜별로 , orders를 기준으로 계산한다.
class PaymentRecalc(TemplateView):
	template_name="index.html"


	def get_context_data(self, **kwargs):
		context = super(PaymentRecalc, self).get_context_data(**kwargs)
		client_list = Orders.objects.filter(received = False).distinct().values('client')

		param_date = str_to_date(self.kwargs['date'])

		Payment.objects.recalc_all(client_list,param_date)
		return context