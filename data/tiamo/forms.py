# -*- coding: utf8 -*-
from django import forms
from tiamo.models import *
import datetime
class PurchaseDetailForm(forms.Form):
    material = forms.ModelChoiceField(empty_label="품명",queryset=Material.objects.all())
    seller = forms.ModelChoiceField(initial="취급처",queryset=Seller.objects.all())
    count = forms.IntegerField(initial='1',help_text="수량")
    now_unit_price = forms.IntegerField(help_text="단가")
    total_price =  forms.IntegerField(help_text="합계")
    comment = forms.CharField()
    orderdate = forms.DateField(initial=datetime.date.today) #,"%y-%m-%d"

    def __init__(self, *args,**kwargs):
        seller = kwargs.pop('seller', None)
        super(PurchaseDetailForm, self).__init__(*args, **kwargs)

        if seller:
            self.fields['material'].queryset = Material.objects.filter(seller_id=seller)
            
            self.fields['seller'].queryset = Seller.objects.filter(id = seller)
            self.fields['seller'].initial=seller

# class OrdersForm(forms.ModelForm):
    # class Meta:
    #     model = Orders
    #     fields = ['client','menuname','order_count','comment']

    #     # Overriding default fields
    #     widgets = {
    #         'client' : forms.TextInput( attrs={'class': 'form-control',}),
    #         'menuname' : forms.TextInput( attrs={'class': 'form-control',}),
    #     }

    #     customize fields
    #     help_texts = {
    #         'menuname': _('투원냉,설반,원대2...')
    #     }
    #     error_messages = {
    #         'menuname' :{
    #             'invaild_string' : _("투원냉,설반,원대2...")
    #         }
    #     }

        