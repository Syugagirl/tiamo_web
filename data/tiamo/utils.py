# -*- coding: utf-8 -*-
import logging
from mst.models import Product, Client
from django.shortcuts import get_object_or_404
from datetime import date 
import datetime as datetime 

logger = logging.getLogger(__name__)

def isNumber(s):
    try:
        float(s)
        return True
    except Exception:
        return False

YUN_DAL= (
(2020,04)
,(2023,02)
,(2025,06)
,(2028,05)
,(2031,03)
,(2033,07)
)
# 원달인가
def is_yun_dal(_date):
    for year, mon in YUN_DAL:
        if year <= _date.year:
            logger.error("***************WARRNING***************")
            logger.error("윤달 추가지정이 필요합니다.....")
        if _date.month == mon and _date.year == year:
            return True
    return False
# date.isoweekday() 월요일=1 ~ 일요일=7
WEEKDAYS = {
        1: '월',
        2: '화',
        3: '수',
        4: '목',
        5: '금',
        6: '토',
        7: '일' }

# 수금 대상이 될 요일을 반환함. 
def get_target_weekday(_date=None):
    day = _date.day
    target_days = []
    target_weekdate = _date.isoweekday()

    #오늘이 일 요일이면 월요일을 반환 
    if target_weekdate == 7:
        target_days.append(1)
    else:
        target_days.append(target_weekdate)
    return target_days

def is_business_day(_date):
    if _date.isoweekday() == 7 :
        return False
    else:
        return True

def not_received_today(_received_date = None):
    if _received_date is None :
        return True
    last_received = _received_date 
    if date.today().month == last_received.month :
        delta = date.today().day - last_received.day
        if delta > 1 :
            return False
    return True
#월말 수금집 검사하는 기간인가 
def is_end_of_month(_date=None):
    tomorrow = get_tomorrow() 
    towdays_later = get_tomorrow(tomorrow)

    
    #이틀뒤가 다음달 1일이고 1일뒤가 영업일이 아니면 오늘은 수금대상임 
    if towdays_later.day == 1 :
        if is_business_day(tomorrow) is False:
            return True 
        

    #하루 뒤가 다음달 1일이면 월말 수금 대상임
    if tomorrow.day == 1:
        return True
    else:
        return False
    # p_start = 29 #월말 표현 시작일
    # p_end = 3    #월말 표현 마지막일

    # if _date is not None:
    #     _date = date.today()

    # if is_yun_dal(_date):
    #     p_start = 27
    # # 27,28  29 30 31
    # # 1,2,3
    # if p_start <= day <= 31:
    #     return True
    # if 1 <= day <= 5:
    #     return True

    # return False



WEEKDAYS = {
        1:'월',
        2: '화',
        3: '수',
        4: '목',
        5: '금',
        6: '토',
        7: '일' }
    
def get_date_name(day_num):
    return WEEKDAYS[int(day_num)]

def get_tomorrow(today = None):
    if today is None:
        today = date.today()

    return today + datetime.timedelta(days=1)

def get_yesterday(today = None):
    if today is None:
        today = date.today()

    return today - datetime.timedelta(days=1)

def str_to_date(_datestr):
    try:
        year  = int(_datestr[0:4])
        month = int(_datestr[4:6])
        day   = int(_datestr[6:])
        
    except Exception, e:
        raise e
        print "날짜 형식이 잘 못 되었습니다 ex)yyyy-mm-dd"
        return HttpResponse(json.dumps(result_data), content_type='application/json')

    try:
        result_date = date(year,month,day)
    except Exception, e:
        raise e
        print"date() type 변환 불가"
    return result_date

 

def getProductset(_menuname):
    
    # , 로 분리한다. 
    # inputdata : '투원냉 3, 투매실 ,반설 1, 반오미자 1-1, 반매실 -1'
    mapped = []


    for  wordset in _menuname.strip().split(','):
        item = wordset.strip().split(' ')
        # 숫자가 생략된 경우, 1을 기본값으로 함
        # 보리반 3 일때 에러남 .

        coupon_txt = ''
        if len(item) > 2:
            logger.error(wordset)
            logger.error("품목명과 수량은 공백 하나로 구분되어야 합니다")
            raise SyntaxError ("품목명과 수량은 공백 하나로 구분되어야 합니다")

        elif len(item) == 1:
            p_name = item[0]
            # logger.info(" 수량 생략됨 %s" % item.pop())
            p_count = 1
            mapped.append ([p_name , p_count, coupon_txt])
        
        elif len(item) == 2:
            p_name = item[0] 
            cnt_set = item[1].split('-')

            if len(cnt_set) > 2:
                logger.error(wordset)
                logger.error("수량은 '-' 한개로만 구분될 수 있습니다 ")
                raise SyntaxError ("수량은 '-' 한개로만 구분될 수 있습니다 ")

            if len(cnt_set) == 2 or len(cnt_set) == 1:
                # no coupon
                if cnt_set[0] != '':
                    if isNumber(cnt_set[0]):
                        p_count = cnt_set[0]
                        mapped.append ([p_name , p_count, coupon_txt])
                    else:
                        logger.error(wordset)
                        logger.error("판매수량은 숫자형식이어야 합니다")
                        raise SyntaxError ("판매수량은 숫자형식이어야 합니다")
                else:
                    logger.debug("p_count was empty")
                
                # has coupon
                if len(cnt_set) == 2:
                    if isNumber(cnt_set[1]):
                        c_count = cnt_set[1]
                        coupon_txt = 'coupon'
                        mapped.append ([p_name , c_count, coupon_txt])
                    else:
                        logger.error(wordset)
                        logger.error("쿠폰수량은 숫자형식이어야 합니다")
                        raise SyntaxError ("쿠폰수량은 숫자형식이어야 합니다")
                 
    result_list = []
    for value in mapped: 
        try: 
            p = Product.objects.get(name = value[0])
        except Exception, e:
            logger.error("%s 을 조회할 수 없습니다. " %  value[0] )
            raise Exception ("메뉴명을 조회할 수 없습니다")
        
        # if coupon
        if value[2] == 'coupon' :
            logger.info( "** cupon added %s - %s (%s)" % (p.name, str(value[1]) ,str(value[2]) )) 
    
        result_list.append( [p, value[1], value[2]] )         
 
    return result_list


# inputdata : '투원냉 3, 투매실 2'
# ouputdata : [  ['투원냉', '3'], ['투매실', '2']  ]
def getProductset_origin(_menuname):
    
    # , 로 분리한다. 
    # inputdata : '투원냉 3, 투매실 2'
    mapped = []
    for  a in _menuname.strip().split(','):
        item = a.strip().split(' ')
        # 숫자가 생략된 경우, 1을 기본값으로 함
        # 보리반 3 일때 에러남 .
        if len(item) == 1: 
            item.append(1) 
            mapped.append ([item[0] , item[1], ''])
        else:
            b = item[1].split('-')

            if isNumber(b[0]):
                mapped.append ([item[0] , b[0], ''])
            if len(b) > 1:
                mapped.append ([item[0] , b[1], 'coupon'])

    result_list = []
    for value in mapped: 
        try: 
            p = Product.objects.get(name = value[0])
        except Exception, e:
            logger.info("%s 을 조회할 수 없습니다. " %  value[0] )
            raise e
        
        # if coupon
        if value[2] == 'coupon' :
            logger.info( "** cupon added %s - %s (%s)" % (p.name, str(value[1]) ,str(value[2]) )) 
	
        result_list.append( [p, value[1], value[2]] )         
 
    return result_list

def getClientByPosNum (_client):
    seg = _client[0:1]
    posnum = _client[1:]
    
    result = Client.objects.get(segment = seg, posnumber = posnum)
    return result

def getClientById (_id):
    return Client.objects.get(id = _id)

def get_total(product_list):
    # product_list =  [  ['투원냉', '3'], ['투매실', '2']  ]
    total = 0
    
    for item in product_list:
        try: 
            p = Product.objects.get(name = item[0])
            if item[2] == 'coupon':
                is_coupon = 0
            else:
                is_coupon = 1
            count = int(item[1])

        except Exception, e:
            logger.error("%s 을 조회할 수 없습니다." % item[0])  
            raise e
        total  += int(p.selling_price) * count * is_coupon
    return total
