# -*- coding: utf-8 -*-
from django.contrib import admin
from tiamo.models import PurchaseDetail, Orders , Payment, Sales


# Register your models here.
class PurchaseDetailAdmin(admin.ModelAdmin):

	list_display=('id','now_unit_price','count','material','total_price','seller','comment','orderdate')
	list_filter=('orderdate',)  
	search_fields=('material__name',)
admin.site.register(PurchaseDetail, PurchaseDetailAdmin)


class OrdersAdmin(admin.ModelAdmin):
	list_display=('id', 'client','client_name' ,'product','count' ,'orderdate'  ,'comment')
	list_filter=( 'orderdate',)
	def client_name(self, obj):
		return obj.client.name
admin.site.register(Orders, OrdersAdmin)

class SalesAdmin(admin.ModelAdmin): 
	list_filter = ( 'orderdate','client')
	list_display =('id','selling_price','applied_coupon','client' ,'orderdate','received')
	def client_name(self, obj):
		return obj.client.name
admin.site.register(Sales, SalesAdmin)
		

class PaymentAdmin(admin.ModelAdmin):
	list_display=( 'id', 'client','client_name','start_date','unpaid_total','applied_coupon','paid_date','remain','last_payday','collection_signal_lv','comment')
	list_filter =( 'paid_date','collection_signal_lv')
	search_fields=('id',)
	def client_name(self, obj):
		return obj.client.name
	def payment_type(self,obj):
		return obj.client.payment_type.name
admin.site.register(Payment, PaymentAdmin)