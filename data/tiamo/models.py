# -*- coding: utf-8 -*-
### CharField 에 null = True 는 권장하지 않는다.

import logging
from django.db import models
from django.db.models import   Max, Min
from django.utils import timezone
from mst.models import Product, Material, Seller, Client
from tiamo.utils import *
from datetime import date 
import datetime as datetime
from model_utils.models    import TimeStampedModel
# 모든 쿼리를 볼 수 있음 
# from django.db import connection



logger = logging.getLogger(__name__)

class PurchaseDetailManager(models.Manager):
     
    def accrued_total(self,sellers, month):
        result_data = []
        year = date.today().year
        month = 4
        for seller in sellers :
            obj={}
            obj['seller'] = seller.name
            obj['total_price'] = 0
            p_qs = PurchaseDetail.objects.filter(orderdate__year= str(2018) ,  orderdate__month= str(01) , seller = seller)
            
            for item in p_qs:
                obj['total_price'] += int(item.total_price)

            result_data.append(obj);
             
        return result_data


        
class PurchaseDetail(TimeStampedModel):
    id = models.AutoField(primary_key=True)
    material =  models.ForeignKey('mst.Material')
    seller = models.ForeignKey('mst.Seller', blank=True, default = 1)
    count = models.CharField(max_length=200, default = 1)
    now_unit_price = models.CharField(max_length=200)
    total_price = models.IntegerField(default = 0)
    comment = models.CharField(max_length=200, blank=True)
    orderdate = models.DateField(default=timezone.now, verbose_name ="주문일자")

    class Meta:
        verbose_name = '*매입한 품목*'
        verbose_name_plural = '매입기록들'
        db_table = 'purchasedetail' 
    
    def get_absolute_url(self):
        return u'/tiamo/purchase/%d/' % self.id 

    def __str__(self):
        return "일자별 매입한 품목"

    objects = PurchaseDetailManager()



class OrdersLogManager(models.Manager):
     
    def insert_orders_log(self, _client , _menuname , _comment, _date):
        client = _client
        if (isNumber(client) == False) :
            client = getClientByPosNum(_client)
        else :
            client = getClientById(_client)

        OrdersLog(client = client, order_txts = _menuname, comment = _comment, orderdate = _date).save()

    def delete_orders_log(self, _client, _date):
        OrdersLog.objects.filter(client = _client, orderdate = _date).delete()

class OrdersLog(TimeStampedModel):
    id = models.AutoField(primary_key=True)
    client = models.ForeignKey('mst.Client', blank=True, verbose_name ="주문처")
    order_txts = models.CharField(max_length=100, blank = False, verbose_name = "판매기록") 
    orderdate = models.DateField(default=timezone.now, verbose_name ="주문일자")
    comment   = models.CharField(max_length=100, blank = True, verbose_name = "특이사항") 

    class Meta:
        verbose_name = '*막장부 입력기록*'
        verbose_name_plural = '막장부'
        db_table = 'orders_log'

    def __str__(self):
        return "일자별 판매기록(막장부)"


    objects = OrdersLogManager()



class OrdersManager(models.Manager):
     
    def makeOrderDataSet(self, _client , _menuname , _comment, _date):
        client = getClientByPosNum( _client)
        product_list = getProductset( _menuname) #output [  ['투원냉', '3'], ['투매실', '2']  ]
        resultData = {'client' : client , 'product_list' : product_list, 'date' : _date}
        return resultData


    # _dataset = { 'client' : client ,'menuname': products
    #            , 'product_list' : product_list, 'date' : target_date
    #            , 'total_price' : total_price, 'coupon_total' : coupon_total}
    def upsertOrderData(self, _dataset):
        for p in _dataset.get('product_list'):
            defaults = { 'count' : p[1]}
            received = False
            if p[2] == 'coupon':
                is_coupon = True
            else:
                is_coupon = False

            try:
                obj = Orders.objects.get(
                    client = _dataset.get('client'), product = p[0], orderdate = _dataset.get('date'), is_coupon=is_coupon, received = received) 

                for key, value in defaults.items():
                    #조회된 obj 에 count 값 누적 
                    setattr(obj, key, int(value) + int(obj.count))
                    
                    obj.save()
            except Orders.DoesNotExist:
                new_obj = Orders.objects.create(
                    client = _dataset.get('client'), product = p[0], count = p[1], orderdate = _dataset.get('date'), is_coupon=is_coupon, received = received)
            except Exception, e:
                raise e
            
            logger.info("Order data updated %s(%d) - %s %d (coupon : %s)" % (_dataset.get('client').name,_dataset.get('client').id , p[0].name, int(p[1]), is_coupon ) );

        # Sales 테이블에 값을 업데이트 한다.
        Sales.objects.propagate_to_daily_order(
                            _dataset.get('date'), _dataset.get('client'))

        # Payment 테이블에 값을 업데이트 한다.
        Payment.objects.propagate_to_payment(_dataset.get('client'))


    def validateMenuNames(self, _menuname):
        product_list = getProductset( _menuname)
        return product_list

    def update_datas(self, client_id , target_date, products_txt):
        # get client
        client = Client.objects.get(id = client_id)

        product_list = getProductset( products_txt) #output [  ['투원냉', '3'], ['투매실', '2']  ]

        total_price = get_total(product_list)

        #TODO applied_coupon 이 없네?!
        order_dataset = {'client' : client, 'product_list' : product_list, 'date' : target_date}

        #TODO: transaction start
            # delete data ranged in orderdate
        
        Orders.objects.filter(orderdate = target_date,client = client, received=False).delete()
            # save data 
 
        Orders.objects.upsertOrderData(order_dataset)
        #TODO: transaction end
        return total_price

    # 기간안에 속하는 모든 거래처의 거래기록을 조회한다.
    def get_OrderData_all(self, start_date, end_date):
        
        total_order_recodes = None
        _clients_list = []
        if end_date is None:
            end_date = start_date

        logger.debug("%s %s" % (start_date, end_date))
        total_order_recodes = Orders.objects.filter(orderdate__range = (start_date, end_date) ).order_by('client__posnumber','client__segment')
        total_order_recodes.prefetch_related('client','product','comment','id','received')

        for item in total_order_recodes:
            if item.client not in _clients_list:
                _clients_list.append(item.client)
        return self.getOrderDatas( start_date, end_date, _clients_list = _clients_list, _datasets= total_order_recodes)



    def get_order_history_by_client_id(self, client, date_until ):
        logger.debug("Orders.get_order_history_by_client_id(client = %s , date_until = %s)" % (client.id, date_until))

        total_order_recodes = None
        result_data = []
        orderdatas = []
        total_order_recodes = Orders.objects.filter(client = client, received = False, orderdate__lte = date_until)

        for order in total_order_recodes:
            if order.orderdate not in orderdatas:
                orderdatas.append(order.orderdate)

                obj = {}
                obj['orderdate'] = order.orderdate.strftime('%Y%m%d')
                obj['products'] = ''
                obj['total_sales'] = 0

                coupon_txt = ''
                if not order.is_coupon :
                    # 합계금액 누적 
                    obj['total_sales'] += int(order.product.selling_price) * order.count
                else:
                    coupon_txt = "(C)"

                if order.count == 1:
                    order.count = ''
                # 판매상품 목록 작성
                obj['products'] += order.product.name + " " + str(order.count) + coupon_txt
                result_data.append(obj)
            else:
                for i in xrange(0,len(result_data)):
                    coupon_txt = ''
                    if result_data[i].get('orderdate') == order.orderdate.strftime('%Y%m%d'):
                        if not order.is_coupon :
                            # 합계금액 누적 
                            result_data[i]['total_sales'] += int(order.product.selling_price) * order.count
                        else:
                            coupon_txt = "(C)"

                        if order.count == 1:
                            order.count = ''
                        # 판매상품 목록 작성
                        result_data[i]['products'] += ", "+order.product.name + " " + str(order.count) + coupon_txt

        return  result_data

    def get_product_text_today(self, client, orderdate):
        product_txt  = ''
        coupon_txt = ''
        order_qs = Orders.objects.filter(client=client, received = False, orderdate = orderdate)
        for item in order_qs :
            if item.count == 1 :
                item.count = ''
            if item.is_coupon :
                coupon_txt = "(C)"
            product_txt +=  item.product.name + " " + str(item.count) + coupon_txt +", "



        return product_txt[:-2] 

    def getOrderDatas(self, start_date, end_date , _clients_list , _datasets):
        logger.debug("start_date = %s, end_date = %s " % (start_date, end_date) )

        result_data=[]
        total_order_recodes = _datasets

        if len(_datasets) == 0 :
            logger.debug("Orders.getOrderDatas() returned. order data is none")
            return None

        # 오늘의 client 목록을 기준으로 표시할 데이터를 만든다.
        for u_client in _clients_list:
            obj = {}
            obj['orderdate'] = start_date.strftime('%Y%m%d')
            if len(_clients_list) >= 1:
                obj['client'] = u_client

            for data in total_order_recodes:
                if u_client.id ==  data.client_id:
                    obj['received'] = data.received
                    if obj.has_key('products') : # 맨 처음이 아니면
                        obj['products'] += ", " 
                    else:
                        obj['products'] = ''
                        obj['total_sales'] = 0

                    coupon_txt = ''
                    # 합계금액 누적 
                    if not data.is_coupon :
                        obj['total_sales'] += int(data.product.selling_price) * data.count
                    else:
                        coupon_txt = "(C)"

                    if data.count == 1:
                        data.count = ''
                    # 판매상품 목록 작성
                    obj['products'] += data.product.name + " " + str(data.count) + coupon_txt
            result_data.append(obj)
            
        return result_data


# 판매기록 요약(외상장부표시용)
class Orders(TimeStampedModel):
    id = models.AutoField(primary_key=True)
    client = models.ForeignKey('mst.Client', blank=True, verbose_name ="주문처")
    product = models.ForeignKey('mst.Product', verbose_name="판매상품")
    count = models.IntegerField(verbose_name = "수량")
    is_coupon = models.BooleanField(verbose_name = "쿠폰사용",default=False)
    received = models.BooleanField(verbose_name = "돈받음",default=False)
    orderdate = models.DateField(default=timezone.now, verbose_name ="주문일자")
    comment   = models.CharField(max_length=200,blank = True, verbose_name = "특이사항") 

    class Meta:
        verbose_name = '*판매한 품목*'
        verbose_name_plural = '막장부 판매기록들'
        db_table = 'orders'

    def __str__(self):
        return "일자별 판매상세"


    objects = OrdersManager()


class Salling_price_Manager(models.Manager):
    use_for_related_fields = True

    def propagate_to_daily_order(self, date, client):
        logger.debug("client - %s  %s " %  (date , client.name))
        total_price =0
        coupon_total = 0
        products_txt = ''
        order_datas = Orders.objects.filter(client = client , orderdate = date , received=False)
        for o_item in order_datas:
            product = Product.objects.get(id= o_item.product_id ) 
            coupon_txt = ''

            if o_item.is_coupon :
                coupon_total += product.selling_price * o_item.count
                coupon_txt = "(C)"

            if len(products_txt) > 2 :
                products_txt += ", "
            products_txt += product.name + " " + str(o_item.count) + coupon_txt

            if o_item.received == False and o_item.is_coupon == False:
                total_price += product.selling_price * o_item.count


        if len(order_datas) != 0:
            self.sales_upsert(date, client, total_price, products_txt, coupon_total )



    def sales_upsert(self, date, client, total_price, products_txt, coupon_total):
        try:
            daily = Sales.objects.get(client = client,orderdate = date, received=False)
            setattr(daily, 'menuname', products_txt)
            setattr(daily, 'selling_price', total_price)
            setattr(daily, 'applied_coupon', coupon_total)
            daily.save()
        except Exception, e:
            Sales.objects.create(
                    client = client,  selling_price = total_price,applied_coupon = coupon_total, orderdate = date, menuname = products_txt)
        
        logger.info("Order-daily data update result : %s %s(%d) - %s %d (coupon : %d)" % (date, client.name, client.id , products_txt, total_price, coupon_total ) );

 


    def get_selling_price(self, client, orderdate):
        selling_price = 0
        try:
            s_item = Sales.objects.get(client = client, orderdate = orderdate)
            selling_price = s_item.selling_price
        except Exception, e:
            pass

        return selling_price

    def get_applied_coupon (self, client, orderdate):
        applied_coupon = 0
        try:
            s_item = Sales.objects.get(client = client, orderdate = orderdate)
            applied_coupon = s_item.applied_coupon
        except Exception, e:
            pass

        return applied_coupon


    # @deprecated
    # 당일 총 판매금액을 업데이트한다. 
    ## Payment 모델의 unpaid_total 에도 누적합산처리한다
    ### _orderDatas data =
    ### [{ 'client': <Client: B4084-1>, 'name': u'B4084-1'
    ### , 'total_sales': 800L,'posnum': u'4084-1',
    ###  'products': u'\ubc18\ub0c9 ', 'orderdate': '20170927'}, ...]
    def update_or_create(self,_date, _orderDatas):
        logger.debug("Sales.update_or_create( %s %s )" % (_date, _orderDatas))
         # orderdate 일자 unpaid_total 금액을 누적합산하여 업데이트한다.
        total_selling_price =0

        for o_item in _orderDatas:
            # 미수금 총액을 Payment 모델에 재계산 요청할지 결정함
            # if o_item.get('client') == 23 :
            s_datas = Sales.objects.filter(client = o_item.get('client'), received=False)
              
            if len(s_datas) == 0:
                #Sales의 당일판매금액 항목을 생성
                obj = Sales.objects.create(
                    client = o_item.get('client'),  selling_price = o_item.get('total_sales'), orderdate = _date)
                total_selling_price += o_item.get('total_sales')
                logger.info("created")
            else :
                for s_item in s_datas:
                    total_selling_price += o_item.get('total_sales')


            q_order = s_datas.aggregate(Min('orderdate'))
            orderdate_min = q_order.get('orderdate__min')

            #수금용 테이블에 unpaid_total 을 누적함.
            Payment.objects.p_upsert(client = o_item.get('client'), orderdate_min = orderdate_min, total_selling_price = total_selling_price , applied_coupon = applied_coupon)
             

    # 날짜에 속하는 모든 거래처의 거래기록을 조회한다.
    # 수금 , 미수금 관계 없이 조회함.
    def get_OrderData_all_clients(self, _date):
        logger.debug("Sales.get_OrderData_all_clients(_date = %s )" % _date)
        total_order_recodes = None
        if _date : 
            total_order_recodes = Sales.objects.filter(orderdate = _date).order_by('client__posnumber','client__segment')
        else:
            logger.warn("get_OrderData_all_clients need FROM_DATE param. but nothing comes in. _date : %s", _date)
            total_order_recodes = Sales.objects.all().limit(100)

        return total_order_recodes 


 
# 일일 판매기록 요약
class Sales(TimeStampedModel):
    id = models.AutoField(primary_key=True)
    client = models.ForeignKey('mst.Client', blank=True, verbose_name ="거래처")
    selling_price = models.IntegerField(verbose_name = "총판매액")
    applied_coupon = models.IntegerField(verbose_name = "쿠폰 적용가")
    orderdate = models.DateField(default=timezone.now, verbose_name ="판매일자")
    received = models.BooleanField(verbose_name = "돈받음",default=False)
    menuname = models.CharField(max_length=400, blank=True, verbose_name = "판매항목 상세")

    class Meta:
        verbose_name = '일일 판매기록 요약'
        verbose_name_plural = '판매기록 요약'
        db_table="orders_daily"

    def __str__(self):
        return "일자별 총 판매액"

    objects = Salling_price_Manager()


PAYMENT_TYPE = (
    (20, '계좌이체'),
    (21, '지정요일'),
    (22, '즉시결제'),
    (23, '익일'),
    (24, '당일'),
    (25, '월말'),
    (26, '제한금액'),
    (30, '임의수금')

    )  
    # payment.payment_rule_sub 값이 날짜인 경우만 필터링 함. 별도로 분류하지 않음 
    # (27, '지정일')

# date.isoweekday() 월요일=1 ~ 일요일=7
WEEKDAYS = {
        1:'월',
        2: '화',
        3: '수',
        4: '목',
        5: '금',
        6: '토',
        7: '일' }
class PaymentManager(models.Manager): 

    # client에 대해 Payment.unpaid_total 을 갱신한다.
    # 총 미수금 합계를 반환한다
    def propagate_to_payment(self, client):
        logger.debug("propagate_to_payment - %d  %s " %  (client.id , client.name))
        total_selling_price = 0
        total_coupon = 0
        sales_recode_exist = False
        s_datas = Sales.objects.filter(client = client, received = False)
        if s_datas:
            sales_recode_exist = True
            for s_item in s_datas:
                total_selling_price += s_item.selling_price
                if s_item.applied_coupon :
                    total_coupon += s_item.applied_coupon

        try:
            p_data = Payment.objects.get( client = client, paid_date__isnull=True)
            setattr(p_data, 'unpaid_total', total_selling_price)
            setattr(p_data, 'applied_coupon', total_coupon)
            
            p_data.save()
        except Exception, e:
            if sales_recode_exist :
                p_item = Payment.objects.filter( client = client, paid_date__isnull=False).aggregate(Max('paid_date'))
                last_payday = p_item.get('paid_date__max')
                Payment.objects.create(
                        client = client, unpaid_total = total_selling_price, applied_coupon = total_coupon, start_date = date.today() , last_payday=last_payday)

        return total_selling_price

    # @deprecated
    # def p_upsert(self, client , orderdate_min, total_selling_price,applied_coupon):
    #     logger.debug("Payment.p_upsert params :")  
    #     logger.debug("client.id - %d \n orderdate_min - %s" % (client.id , orderdate_min))
    #     logger.debug("total_selling_price - %d ", total_selling_price)

    #     comment = ''

    #     try:
    #         # Payment 항목은 복수개 존재할 수 없다. 
    #         ## 미수금 잔액이 있는 경우 (payment.start_date == null)
    #         ## 판매이력이 발생한 경우 (payment.start_date == orderdate_min)
    #         p_qs = Payment.objects.filter(client = client, paid_date__isnull= True)
    #         p_lastest = p_qs.aggregate(Max('paid_date'))
    #         last_payday = p_lastest.get('payday__max')
            
    #         if len(p_qs) == 0:
    #             p_new_obj = Payment.objects.create(
    #                     client = client, unpaid_total = total_selling_price
    #                     , start_date = orderdate_min
    #                     , last_payday = last_payday)
    #             setattr(p_new_obj, 'unpaid_total', total_selling_price)
    #             p_new_obj.save()
    #             return 0
            

    #         # 미수금 잔액(remain)이 있는 payment가 존재 하고, 판매가 발생 한 경우
    #         # 판매발생 분과 합친다
    #         pre_remain = 0
    #         for obj in p_qs:
    #             setattr(obj, 'comment', comment )
    #             setattr(obj, 'start_date', orderdate_min )
    #             setattr(obj, 'unpaid_total', total_selling_price)
    #             setattr(obj, 'applied_coupon', applied_coupon)

    #             obj.save()

    #     except Exception , e:
    #         query = "Payment.objects.filter(client = %s, paid_date__isnull= True)" % client
    #         logger.error('%s (%s)' % (e.message, type(e)))
    #         logger.error("%s" % query)
    #         logger.info("Payment.p_upsert() error occered. %s ", query)
    #         raise e


    def unpaidList_filterd( self, target_date):
        logger.debug("Payment.unpaidList_filterd( target_date = %s )" %  target_date)

        if target_date == None :
            target_date = date.today()
        else:
            # target_date = get_yesterday(target_date)
            logger.debug("unpaid list filtered by date - %s" % target_date)

        # 수금정책 필터
        ## TODO: DB에서 가져와 초기화 할 것 
        ### 추가나 삭제가 되었을 때 에러가 나지 않을 것
        rule_types = []
        for r_id, r_name in PAYMENT_TYPE:
            rule_types.append(r_id)

        # 지정요일은 제외 
        rule_types.remove(21)

        # 요일을 지정한 경우 필터링 조건 추가
        t_weekdays = get_target_weekday(target_date)

        filterd_clients_qs = Client.objects.filter( 
            payment_type__in = rule_types)  | Client.objects.filter( payment_rules__in = t_weekdays) | Client.objects.filter( payment_reserv_date__isnull=False)
        dataset = Payment.objects.filter(paid_date__isnull=True, client__in = filterd_clients_qs).order_by('client__segment','client__posnumber')
        

        filterd_clients = []
        for p_item in dataset:

            # 지정일이 있인 경우
            if p_item.client.payment_reserv_date is not None:
                try:
                    p_date = p_item.client.payment_reserv_date
                    if get_yesterday(p_date) <= target_date:
                        filterd_clients.append(p_item)
                        logger.debug(" %d - 예약된 수금지정일 도래 " % (p_item.client_id   ))
                    # if isinstance(p_date, datetime.date):
                    # else:
                    #     logger.error("***************Warrning!!!***************")
                    #     logger.error(" %d - %s 의 지정일 수금에 대한 날짜 지정이 올바르지 않습니다" % (p_item.client_id , p_item.client.name ))

                except Exception, e:
                    logger.error('check data. Payment client.id = %d ' % p_item.client.id)
                    logger.error('%s (%s)' % (e.message, type(e)) )
                    pass
            # 월말
            elif p_item.client.payment_type_id == 25:

                s_item = Sales.objects.filter(orderdate__lt = date.today(), client = p_item.client.id, received = False).values('selling_price')
                if len(s_item) >= 1:
                    # 영업일이고 
                    # if is_business_day(target_date):
                    # print p_item.client.name
                    # 월말이면 
                    if is_end_of_month(target_date): 
                        # print "월말"
                        filterd_clients.append(p_item) 

            # 계좌이체 
            elif p_item.client.payment_type_id == 20:

                s_item = Sales.objects.filter(orderdate__lt = date.today(), client = p_item.client.id, received = False).values('selling_price')
                if len(s_item) >= 1:
                    filterd_clients.append(p_item) 

            #  즉시결제 인 경우 
            elif p_item.client.payment_type_id == 22:
                filterd_clients.append(p_item)

            # 익일 인 경우 
            elif p_item.client.payment_type_id == 23:

                s_item = Sales.objects.filter(orderdate__lt = date.today(), client = p_item.client.id, received = False).values('selling_price')
                if len(s_item) >= 1:
                    filterd_clients.append(p_item)

            # 당일 인 경우 
            elif p_item.client.payment_type_id == 24:
                if p_item.last_payday :
                    if p_item.last_payday != date.today():
                        filterd_clients.append(p_item)
                else:
                    filterd_clients.append(p_item)

            # 제한금액 인 경우 
            elif p_item.client.payment_type_id == 26:
            
                # 어제까지 판매한 금액이 제한금액을 넘으면 
                #미수금 총액
                if p_item.unpaid_total == None : p_item.unpaid_total = 0
                if p_item.remain == None : p_item.remain = 0
                #오늘 판매한 금액
                today_sales = 0
                s_item = Sales.objects.filter(orderdate = date.today(), client = p_item.client, received = False).values('selling_price')
                if s_item :
                    if s_item[0]['selling_price'] :
                        today_sales = s_item[0]['selling_price']
                unpaid_total = p_item.unpaid_total + p_item.remain - today_sales


                if p_item.client.payment_rules_id is None:
                    logger.error("***************ERROR***************")
                    logger.error("%s - 의 수금 제한금액이 설정되지 않았습니다!!! " % str(p_item.client_id ))
                elif int(p_item.client.payment_rules.name) <= unpaid_total :
                    filterd_clients.append(p_item)
            # 지정요일 
            elif p_item.client.payment_type_id == 21:

                s_item = Sales.objects.filter(orderdate__lt = date.today(), client = p_item.client.id, received = False).values('selling_price')
                if len(s_item) >= 1:
                    p_item.client.payment_rule_sub = "-"+ get_date_name(p_item.client.payment_rules_id)
                    filterd_clients.append(p_item) 
                
            # 임의수금 
            elif p_item.client.payment_type_id == 30:
                s_item = Sales.objects.filter(orderdate__lt = date.today(), client = p_item.client.id, received = False).values('selling_price')
                if len(s_item) >= 1:
                    filterd_clients.append(p_item)

            else:
                logger.error("***************Warrning!!!***************")
                logger.error("수금정책을 확인하여 기본값을 입력하세요:" )
                logger.error("client id : " + str(p_item.client.id) )
                logger.error("client name : "+ p_item.client.name)
                logger.error("정책이 설정되지 않으면, 수금대상에 표시되지 않습니다." )
                logger.error(" 수금 정책 설정 필요 c_id - %d" % p_item.client.id)
        return self.make_unpaid_datas(target_date, filterd_clients) 


    #마지막 돈 준날 부터 endday까지 최종 판매액을 구한다.
    # endday 까지의 모든 외상리스트 반환
    def unpaidList_all( self,  target_date = None, _client = None) :
        logger.debug("Payment.unpaidList_all( %s , %s)" %  (target_date, _client) )

        # 반환되는 데이터목록
        if _client:
            dataset = Payment.objects.filter(paid_date__isnull=True, client = _client)
        else:
            dataset = Payment.objects.filter(paid_date__isnull=True).order_by('client__segment','client__posnumber')

        return self.make_unpaid_datas( target_date, dataset) 


    def make_unpaid_datas(self,  target_date, payment_datas):
        logger.debug("Payment.make_unpaid_datas( %s )" %  (target_date))
        result_data = []
        for p_item in payment_datas:
            obj={}
            obj['id'] = p_item.client_id
            obj['client'] = p_item.client
            #미수금 총액
            if p_item.unpaid_total == None :
                p_item.unpaid_total = 0
            if p_item.remain == None : 
                p_item.remain = 0
            
            obj['unpaid_total'] = p_item.unpaid_total + p_item.remain

            s_item = Sales.objects.filter(orderdate = date.today(), client = p_item.client, received = False).values('selling_price')
            #어제까지 미수금
            obj['unpaid_until_yesterday'] = 0
            if obj['unpaid_total'] : 
                 obj['unpaid_until_yesterday'] = int(obj['unpaid_total'])
            #오늘 미수금
            obj['unpaid_today'] = 0
            if s_item :
                if s_item[0]['selling_price'] :
                    obj['unpaid_until_yesterday'] = obj['unpaid_until_yesterday'] - s_item[0]['selling_price']
                obj['unpaid_today'] = s_item[0]['selling_price']


            obj['applied_coupon'] = p_item.applied_coupon
            #주문 시작일자
            obj['start_orderdate'] =  p_item.start_date
            #지난 수금일
            obj['last_payday'] = p_item.last_payday
            #마지막 거래일자 
            # obj['last_orderdate'] == None 인 경우는 판매기록이 없는 경우 (= 수금후 잔액만 남고 주문 한 기록이 아직 없음.)
            last_order =  Sales.objects.filter(client = p_item.client, received = False).aggregate( Max('orderdate')  )

            obj['last_orderdate'] =  last_order.get('orderdate__max')

            result_data.append (obj)
        return result_data

    def paidList( self, payday ):
        logger.debug("Payment.paidList( %s )" % payday)
        p_datas = Payment.objects.filter( created__gte =  payday).order_by('modified')
        return p_datas


   # 수금처리
    # 요청받은 날짜 기준으로 어제까지의 미수금을 수금처리함
    def received_untill(self, client, payday):
        logger.debug("Payment.objects.received_untill(  %d , %s)" % (client.id, payday ))
        #수금완료처리할 금액
        received_total = 0

        #미수금 총액 
        unpaid_total = 0
        #잔액 
        remain = 0
        comment = str(payday) + "까지의 미수금 받음"


        Sales.objects.filter( client = client.id, orderdate__lte = payday, received = False).update(received = True)
        Orders.objects.filter( client = client.id, orderdate__lte = payday).update(received = True)

        p_data = Payment.objects.get(client = client, paid_date__isnull = True)

        setattr(p_data, 'comment',  comment)   
        setattr(p_data, 'paid_date',  payday)
        p_data.save()

        #수금일 지정했던 것이 있다면 삭제한다.
        setattr(client, 'payment_reserv_date', None)
        client.save()

        remain = self.propagate_to_payment(client)
        return remain
        

    # @deprecated
    # 수금처리
    # 요청받은 날짜 기준으로 어제까지의 미수금을 수금처리함
    # def received_untill_original(self, client, payday):
    #     logger.debug("Payment.objects.received_untill(  %d , %s)" % (client.id, payday ))
    #     #수금완료처리할 금액
    #     received_total = 0

    #     #미수금 총액 
    #     unpaid_total = 0
    #     #잔액 
    #     remain = 0
    #     comment = str(payday) + "까지의 미수금 받음"
        
    #     p_data = Payment.objects.get(client = client, paid_date__isnull = True)

    #     # 남은 미수금 총액 계산 >>>>>
    #     if p_data.unpaid_total is not None:
    #         unpaid_total += p_data.unpaid_total
    #     else:
    #         logger.warn("미수금이 존재하지 않는데, 수금요청을 하였음. 데이터 확인 Payment client.id : %d" % client.id)
    #         return "판매기록이 없어서 처리안함."
            
    #     if p_data.remain is not None:
    #         unpaid_total += p_data.remain
    #     # 남은 미수금 총액 계산 <<<<<


    #     sales_qs = Sales.objects.filter( client = client.id, orderdate__lte = payday, received = False).values('selling_price')
    #     for s_item in sales_qs:
    #         received_total += s_item.selling_price

    #     sales_qs.update(received = True)
    #     Orders.objects.filter( client = client.id, orderdate__lte = payday).update(received = True)

    #     # TODO: payment.start_date 가 있어야 하는지 모르겠다
    #     # 오늘 주문한 기록이 있으면 start_date를 오늘 날짜로 지정한다.
    #     try:
    #         sales_item = Sales.objects.get(client= client , orderdate = payday)
    #         start_date = payday
    #     except Exception, e:
    #         sales_item = None
    #         start_date = None
    #         pass

    #     # 잔액계산
    #     remain = unpaid_total - received_total 

    #     if remain : 
    #         Payment.objects.create(
    #                 client = client, unpaid_total= remain, last_payday = payday, start_date = start_date,applied_coupon = 0)

    #     setattr(p_data, 'comment',  comment)   
    #     setattr(p_data, 'paid_date',  payday)
    #     p_data.save()

    #     #수금일 지정했던 것이 있다면 삭제한다.
    #     setattr(client, 'payment_reserv_date', None)
    #     client.save()
    #     return remain

    ### TODO : 트랜잭션이 필요함
    # 받은 금액만큼만 수금처리 
    def received_by_paid_amount(self, client, paid_amount, payday):
        p_data = None
        logger.debug("Payment.objects.received(  %d %s %s )" % (client.id , paid_amount, payday))  

        try:
            p_data = Payment.objects.get(client_id = client.id, paid_date__isnull= True)
        except Exception, e:
            query = "Payment.objects.get(client_id = %d, paid_date__isnull= True)" % client.id
            logger.error('%s (%s)' % (e.message, type(e)) )
            logger.error("Payment.objects.received() error occered. %s ", query)
            raise e


        setattr(p_data, 'paid_date',  payday)
        setattr(p_data, 'last_payday',  date.today())
        
        unpaid = 0
        # 미수금 총액보다 받은 돈이 적은 경우 _추가처리 
        # remain(잔액), comment 생성 
        if not None == getattr(p_data, 'unpaid_total') :
            unpaid  +=  getattr(p_data, 'unpaid_total') 
        if not None == getattr(p_data, 'remain') :
            unpaid  +=  getattr(p_data, 'remain') 


        
        msg = "수금금액 "+ str(paid_amount)  
        setattr(p_data, 'comment', msg)
        p_data.save()
            
        remain = unpaid - paid_amount
        if remain != 0 :
            msg = str(payday)+'까지 수금 후 잔액 '+ str(remain)
            #수금일자 기준으로 미수금을 추가 기록한다.
            Payment.objects.create(
                client = client, unpaid_total = 0, applied_coupon = 0, remain= remain, comment = msg, start_date= date.today(), last_payday = date.today())
            

        Sales.objects.filter( client = client.id).update(received = True)
        Orders.objects.filter( client = client.id).update(received = True)
        
        #수금일 지정했던 것이 있다면 삭제한다.
        setattr(client, 'payment_reserv_date', None)
        client.save()
        return remain

    ## @deprecated
    ### TODO : 트랜잭션이 필요함
    # 받은 금액만큼만 수금처리 
    # def received_by_paid_amount_origin(self, client, paid_amount, payday):
    #     try:
    #         logger.debug("Payment.objects.received(  %d %s %s )" % (client.id , paid_amount, payday))  
    #         p_data = Payment.objects.get(client_id = client.id, paid_date__isnull= True)

    #         #수금일 지정했던 것이 있다면 삭제한다.
    #         setattr(client, 'payment_reserv_date', None)
    #         client.save()

    #         obj = p_data
    #         setattr(obj, 'paid_date',  payday)

    #         unpaid = 0
    #         remain_tag = 0
    #         # 미수금 총액보다 받은 돈이 적은 경우 _추가처리 
    #         # remain(잔액), comment 생성 
    #         if not None == getattr(obj, 'unpaid_total') :
    #             unpaid  +=  getattr(obj, 'unpaid_total') 
    #         if not None == getattr(obj, 'remain') :
    #             remain_tag = 1
    #             unpaid  +=  getattr(obj, 'remain') 

    #         remain = unpaid - paid_amount
    #         if remain != 0 :
    #             msg = str(payday)+'까지 수금 후 잔액 '+ str(remain)
    #             #수금일자 기준으로 미수금을 추가 기록한다.
    #             Payment.objects.create(
    #                 client = client, remain= remain, comment = msg, start_date= payday, last_payday = payday)
    #         if remain_tag :
    #             msg = "수금금액 "+ str(paid_amount)  
    #             setattr(obj, 'comment', msg)

    #         obj.save()
    #     except Exception, e:
    #         query = "Payment.objects.get(client_id = %d, paid_date__isnull= True)" % client.id
    #         logger.error('%s (%s)' % (e.message, type(e)) )
    #         logger.info("Payment.objects.received() error occered. %s ", query)
    #         raise e

    #     Sales.objects.filter( client = client.id, received = False).update(received = True)
    #     Orders.objects.filter( client = client.id, received = False).update(received = True)
    #     return remain


    def remain(self, client):
        remain = 0
        try:
            p_data = Payment.objects.get(client = client, paid_date__isnull = True)
            remain = p_data.remain
        except Exception, e:
            logger.error('%s (%s)' %( e.message, type(e)) )
            pass


        return remain


    def last_payday(self, client):
        last_payday = None
        try:
            p_data = Payment.objects.get(client = client, paid_date__isnull = True)
            last_payday = p_data.last_payday
        except Exception, e:
            logger.error('%s (%s)' %( e.message, type(e)) )
            pass

        return last_payday


    # target_date 까지만 미수금 총액을 구한다.
    def until_yesterday_unpaid(self, client ,target_date):
        unpaid_total = 0

        sales_datas = Sales.objects.filter(client = client, orderdate__lte = target_date, received = False)
        for s_item  in sales_datas:
            unpaid_total += s_item.selling_price

        return unpaid_total


    def get_item(self, client):
        try:
            p_data = Payment.objects.get(client=client, paid_date__isnull = True)
        except Exception, e:
            return None
        return p_data

    def recalc_all(self, _client_list, _date = None):
        if _date is None:
            _date = date.today()
            
        for c_item in _client_list:
            client = Client.objects.get(id = c_item['client'])
            if client.id == 213:

                print _date
                    # Sales 테이블에 값을 업데이트 한다.
            Sales.objects.propagate_to_daily_order(
                            _date, client)

            self.propagate_to_payment(client)


# 외상장부(수금기록)
class Payment(TimeStampedModel):
    id = models.AutoField(primary_key=True)
    client = models.ForeignKey('mst.Client', blank=True, verbose_name ="거래처")
    paid_date = models.DateField(blank=True,  verbose_name="수금일자")
    start_date = models.DateField(blank=False,  verbose_name="미수금 시작일자")
    last_payday = models.DateField(blank=False,  verbose_name="지난 수금일자")
    unpaid_total = models.IntegerField(verbose_name = "미수금총액") 
    applied_coupon = models.IntegerField(verbose_name = "쿠폰 적용가")
    remain = models.IntegerField(verbose_name = "수금 후 잔액", default = 0) 
    collection_signal_lv = models.IntegerField(verbose_name = "수금알림 레벨표시")
    comment   = models.CharField(max_length=200, blank = True, verbose_name = "특이사항") 

    class Meta:
        verbose_name = '수금기록'
        verbose_name_plural = '수금기록들'
        db_table = 'payment'

    def __str__(self):
        return "일자별 수금관리"

    objects = PaymentManager()
