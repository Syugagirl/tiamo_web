
var l_client_data;		 // 거래처 마스터데이터 
var l_client_posnum; // 거래처 동호수 리스트 
 




//ENTER 안먹게 하는것 
function captureReturnKey(e) { 
	// && (e.srcElement.type != 'textarea')
    if(e.keyCode==13 && e.target.id == 'btn_addRaw'){
    	addRaw()
    }
    // console.log(e.target.id)
    if(e.keyCode==13){
    	// console.log("return falsed.11")
    	return false
    } 
};



// id = menuname 에 대한 길이체크 수행
function lengthCheck(myId){
 	// var currentId = this.attr("id");
 	if ($('#'+myId).val().length >= 35){
 		$('#lengthWarnning').show();
 		$('#msg_default').hide();
 	}
 	else if ($('#'+myId).val().length <3 ){
		$('#lengthWarnning').hide(); 
		$('#validationError_msg').hide();
		$('#'+myId+'Error_span').hide();	
		$('#msg_default').show();
 	} 
 	else{
 		$('#lengthWarnning').hide(); 
 		$('#msg_default').show();
 	}
}



function deleteRaw(){
	// formData에 마지막 행 한줄을 삭제한다.
	var tbody = document.getElementById('tbody');
	if (tbody.rows.length < 2) return;
	tbody.deleteRow(tbody.rows.length-1);
}



//오늘 날짜 가져오기
//결과: 2017-08-01
function getTodayDate(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {  dd = '0'+dd } 
	if(mm<10) { mm = '0'+mm  } 
	today = yyyy + '-' +  mm + '-' +dd  ;
	return today
}





// 행 추가 
function addRaw(){ 
	// formData 마지막에 행 한줄을 추가한다.
	var tbody = document.getElementById('tbody');
	var trow = tbody.insertRow(tbody.rows.length);
	var cell0 = trow.insertCell(0);
	var num  = tbody.rows.length-1 
	var newMenunameID = "menuname_" + num
	var newClientID =  "client_"+ num
	var newClientlabel = "lb_"+ num
	cell0.innerHTML =  "<input id=\""+ newClientID +"\"  name= \"client[]\" type=\"text\"  maxlength=\"6\" size=\"6\" >"
	cell0.innerHTML += "<label id=\""+ newClientlabel +"\"></label>" 
	var cell1 = trow.insertCell(1);
	cell1.innerHTML =  "<input id=\""+ newMenunameID +"\" class=\"products\" name= \"menuname[]\" type=\"text\" maxlength=\"50\" size=\"50\">"
	cell1.innerHTML += "<span id=\"span_"+ num +"_Error\" class=\"glyphicon glyphicon-remove\" style=\"display:none\"></span>"
	cell1.innerHTML += "<span id=\"span_"+ num +"_Valid\" class=\"glyphicon glyphicon-ok\" style=\"display:none\"></span> "
	cell1.innerHTML += "<span id=\"total_"+ num+"\"></span>"
	/// call autocomplate
 	$('#'+newClientID).autocomplete({
		source: l_client_posnum
	});

	// add event
	$('#'+newMenunameID).blur(function(){
		menunameCheck (num);
	}).keyup(function() {
	    lengthCheck(newMenunameID);
	});

	$('#'+newClientID).blur(function(){
		add_client_label(newClientID, newClientlabel);

	});

};




